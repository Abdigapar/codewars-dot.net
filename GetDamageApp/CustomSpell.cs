﻿namespace GetDamageApp
{
    public class CustomSpell : AOESpell
    {
        protected override float GetDamage(Unit unity)
        {
            return (unity.Health * 50) / 100;
        }
    }
}