﻿namespace GetDamageApp
{
    public abstract class AOESpell
    {
        public void Apply(Unit[] units)
        {
            foreach (Unit unit in units)
            {
                unit.ApplyDamage(GetDamage(unit));
            }
        }

        protected abstract float GetDamage(Unit unity);
    }
}