﻿using System;

namespace GetDamageApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var units = new Unit[]
            {
                new Unit(200),
                new Unit(455)
            };

            foreach (var unit in units)
            {
                Console.WriteLine($" Unit - {unit.Id} ; Health before {unit.Health} ");
            }

            var spell = new CustomSpell();

            spell.Apply(units);

            foreach (var unit in units)
            {
                Console.WriteLine($" Unit - {unit.Id} ; Health after {unit.Health}");
            }

            Console.ReadKey();
        }
    }
}
