﻿namespace GetDamageApp
{
    public class Unit
    {
        private float _health;
        private static int _id;
        public int Id;

        public float Health => _health;

        public Unit(float health)
        {
            Id = ++_id;
            _health = health;
        }

        public void ApplyDamage(float damage)
        {
            _health -= damage;
        }
    }
}