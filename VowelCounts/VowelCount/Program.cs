﻿using System;
using System.Linq;

namespace VowelCount
{
   public class Program
    {
        public static int GetVowelCount(string str)
        {
            return str.Count(i => "aeiou".Contains(i));
        }
        static void Main(string[] args)
        {
            var str = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book";
            Console.WriteLine(GetVowelCount(str));
        }
    }
}