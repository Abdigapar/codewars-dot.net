﻿using NUnit.Framework;

namespace VowelCountTest
{
    [TestFixture]
    public class VowelCountTest
    {
        [Test]
        public void TestCase1()
        {
            Assert.AreEqual(5, VowelCount.Program.GetVowelCount("abracadabra"), "Nope!");
        }

        [Test]
        public void TestNull()
        {
            Assert.AreEqual(0, VowelCount.Program.GetVowelCount(""), "Nope!");
        }
    }

}
